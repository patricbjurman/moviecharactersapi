﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;


namespace MovieCharactersAPI.Models
{
    public class MovieCharactersDbContext : DbContext
    {
        // Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<MovieCharacter> MovieCharacter { get; set; }

        public MovieCharactersDbContext([NotNullAttribute] DbContextOptions<MovieCharactersDbContext> options)
            : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Set up M2M MovieCharacter.
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });

            // Set up O2M franchise-movies.
            modelBuilder.Entity<Movie>()
                .HasOne(m => m.Franchise)
                .WithMany(f => f.Movies)
                .HasForeignKey(m => m.FranchiseId);

            // Seed data
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 1, Title = "Movie1", FranchiseId = 3 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 2, Title = "Movie2", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie { MovieId = 3, Title = "Movie3", FranchiseId = 1 });

            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 1, FullName = "NamnNamn1" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 2, FullName = "NamnNamn2" });
            modelBuilder.Entity<Character>().HasData(new Character { CharacterId = 3, FullName = "NamnNamn3" });

            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 1, Name = "Franchise1" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 2, Name = "Franchise2" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { FranchiseId = 3, Name = "Franchise3" });

            // Seed M2M MovieCharacter.
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { CharacterId = 1, MovieId = 1 });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { CharacterId = 1, MovieId = 2 });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { CharacterId = 1, MovieId = 3 });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { CharacterId = 2, MovieId = 1 });
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter { CharacterId = 3, MovieId = 1 });
        }
    }
}
