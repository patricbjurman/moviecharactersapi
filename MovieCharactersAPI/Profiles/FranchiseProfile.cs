﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise<->MovieCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();
            // Franchise<->MovieEditDTO
            CreateMap<Franchise, FranchiseEditDTO>()
                .ReverseMap();
            // Franchise<->MovieReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ReverseMap();
        }
    }
}
