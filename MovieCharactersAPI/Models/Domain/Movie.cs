﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models
{
    public class Movie
    {
        // Pk
        public int MovieId { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(255)]
        public string PictureUrl { get; set; }
        [MaxLength(255)]
        public string TrailerUrl { get; set; }
        // Relationships
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public virtual ICollection<MovieCharacter> Characters { get; set; }
    }
}